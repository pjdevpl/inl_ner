package pl.edu.pjatk.s10888.inl.pos.components.pipeline.variants

import org.cleartk.ml.feature.extractor.CleartkExtractor
import org.cleartk.ml.feature.extractor.CombinedExtractor1
import org.cleartk.ml.feature.extractor.CoveredTextExtractor
import org.cleartk.ml.feature.function.*
import org.cleartk.ml.feature.extractor.TypePathExtractor
import org.cleartk.token.type.Token


class FeatureSetJ : FeatureSet {

    override val tokenFeatureExtractor = FeatureFunctionExtractor(
            CombinedExtractor1(CoveredTextExtractor<Token>(), TypePathExtractor(Token::class.java, "pos")),
            LowerCaseFeatureFunction(),
            CapitalTypeFeatureFunction(),
            CharacterNgramFeatureFunction(CharacterNgramFeatureFunction.Orientation.RIGHT_TO_LEFT, 0, 2),
            CharacterNgramFeatureFunction(CharacterNgramFeatureFunction.Orientation.RIGHT_TO_LEFT, 0, 3))

    override val contextFeatureExtractor = CleartkExtractor<Token, Token>(
            Token::class.java,
            FeatureFunctionExtractor(
                    CombinedExtractor1<Token>(
                            CoveredTextExtractor<Token>(),
                            TypePathExtractor(Token::class.java, "pos")),
                    CapitalTypeFeatureFunction()
            ),
            CleartkExtractor.Preceding(1),
            CleartkExtractor.Following(1))
}