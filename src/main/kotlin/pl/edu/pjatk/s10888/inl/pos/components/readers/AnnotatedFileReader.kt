package pl.edu.pjatk.s10888.inl.pos.components.readers

import pl.edu.pjatk.s10888.inl.pos.model.AnnotatedCorpusEntry
import java.io.File

class AnnotatedFileReader(private val file: File) {

    fun readLinesFromCorpus(): List<AnnotatedCorpusEntry> {
        return file.readLines().dropLast(1).filter { line -> !line.contains("ann_morphosyntax.xml") }.map { it: String ->
            val splittedLine = it.split("\t")
            AnnotatedCorpusEntry(splittedLine[0], splittedLine[1], splittedLine[2])
        }
    }

    fun readLinesFromTaggingOutput(): List<AnnotatedCorpusEntry> {
        return file.readLines().dropLast(1).map { it: String ->
            val splittedLine = it.split("\t")
            AnnotatedCorpusEntry(splittedLine[0], null, splittedLine[1])
        }
    }


}