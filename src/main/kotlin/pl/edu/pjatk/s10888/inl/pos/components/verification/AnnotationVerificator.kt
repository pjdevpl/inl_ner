package pl.edu.pjatk.s10888.inl.pos.components.verification

import pl.edu.pjatk.s10888.inl.pos.components.readers.AnnotatedFileParser
import pl.edu.pjatk.s10888.inl.pos.components.readers.AnnotatedFileReader
import pl.edu.pjatk.s10888.inl.pos.components.readers.AnnotatedFileSelector
import pl.edu.pjatk.s10888.inl.pos.components.transformers.CorpusTransformer.Companion.nonNamedEntityTag
import pl.edu.pjatk.s10888.inl.pos.model.AnnotatedCorpusEntry
import pl.edu.pjatk.s10888.inl.pos.model.DetailedMetrics
import pl.edu.pjatk.s10888.inl.pos.model.Metrics
import pl.edu.pjatk.s10888.inl.pos.model.VerificationOutput
import java.io.File

class AnnotationVerificator {

    fun verify(inputFile: File, percentageStart: Double, percentageEnd: Double, classificationOutput: File): VerificationOutput {
        val testData = readTestData(inputFile, percentageStart, percentageEnd)
        val outputData = readOutputData(classificationOutput)
        if (testData.size != outputData.size) {
            throw RuntimeException("Test data and output data varies in size. Check the testing specification")
        }

        val allData = testData.mapIndexed { i, e -> Pair(e, outputData[i]) }

        val allLabels = testData.map { it.namedEntityTag }.toSet().minus(nonNamedEntityTag)
        val detailedMetricsMap = mutableMapOf<String, DetailedMetrics>()
        val metricsMap = mutableMapOf<String, Metrics>()
        allLabels.forEach { label ->
            detailedMetricsMap[label] = DetailedMetrics(0, 0, 0, 0)

            allData.filter { it.first.namedEntityTag == label }.forEach {
                if (it.first.namedEntityTag == it.second.namedEntityTag) {
                    detailedMetricsMap.compute(label) { _, value -> value?.copy(tp = value.tp + 1) }
                } else {
                    detailedMetricsMap.compute(label) { _, value -> value?.copy(fn = value.fn + 1) }
                }
            }

            allData.filter { it.second.namedEntityTag == label }.forEach {
                if (it.first.namedEntityTag != it.second.namedEntityTag) {
                    detailedMetricsMap.compute(label) { _, value -> value?.copy(fp = value.fp + 1) }
                }
            }

            val m = detailedMetricsMap[label]!!
            metricsMap[label] = Metrics(
                    precision = if (m.tp + m.fp > 0) m.tp.toDouble() / (m.tp + m.fp).toDouble() else 0.0,
                    recall = if ((m.tp + m.fn) > 0) m.tp.toDouble() / (m.tp + m.fn).toDouble() else 0.0
            )
        }

        val averagePrecision = metricsMap.values.map { it.precision }.average()
        val averageRecall = metricsMap.values.map { it.recall }.average()
        val f1 = (2 * averagePrecision * averageRecall) / (averagePrecision + averageRecall)

        return VerificationOutput(detailedMetricsMap, metricsMap, averagePrecision, averageRecall, f1)
    }

    private fun readTestData(inputFile: File, percentageStart: Double, percentageEnd: Double): List<AnnotatedCorpusEntry> {
        val testDataReader = AnnotatedFileReader(inputFile)
        val parser = AnnotatedFileParser(testDataReader.readLinesFromCorpus())
        val selector = AnnotatedFileSelector(parser.recognizeSentences())
        val selectedSentences = selector.selectPercentage(percentageStart, percentageEnd)
        return selectedSentences.fold(mutableListOf()) { acc, annotatedSentence -> acc.addAll(annotatedSentence.annotatedWords); acc }
    }

    private fun readOutputData(classificationOutput: File): List<AnnotatedCorpusEntry> {
        val outputReader = AnnotatedFileReader(classificationOutput)
        val parser = AnnotatedFileParser(outputReader.readLinesFromTaggingOutput())
        return parser.recognizeSentences().flatMap { it.annotatedWords }
    }

}