package pl.edu.pjatk.s10888.inl.pos.model

data class AnnotatedCorpusEntry(
        val word: String,
        val posTag: String?,
        val namedEntityTag: String,
        val indexStart: Int = 0,
        val indexEnd: Int = 0) {

    fun withSentencePosition(indexStart: Int, indexEnd: Int): AnnotatedCorpusEntry {
        return this.copy(indexStart = indexStart, indexEnd = indexEnd)
    }

}