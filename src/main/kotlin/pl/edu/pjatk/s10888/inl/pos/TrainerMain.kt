package pl.edu.pjatk.s10888.inl.pos

import com.google.common.base.Stopwatch
import com.google.gson.GsonBuilder
import org.apache.commons.lang.time.DurationFormatUtils
import org.apache.uima.UIMAFramework
import org.cleartk.ml.jar.Train
import org.apache.uima.fit.pipeline.SimplePipeline
import org.apache.uima.fit.component.ViewCreatorAnnotator
import org.apache.uima.fit.factory.AnalysisEngineFactory
import org.apache.uima.fit.factory.AggregateBuilder
import org.cleartk.util.cr.UriCollectionReader
import org.apache.uima.util.Level
import org.cleartk.ml.crfsuite.CrfSuiteWrapper
import pl.edu.pjatk.s10888.inl.pos.components.pipeline.CorpusReader
import java.io.File
import org.cleartk.ml.jar.JarClassifierBuilder
import org.cleartk.opennlp.tools.SentenceAnnotator
import org.cleartk.token.tokenizer.TokenAnnotator
import pl.edu.pjatk.s10888.inl.pos.components.pipeline.AnnotatedOutputWriter
import pl.edu.pjatk.s10888.inl.pos.components.pipeline.NamedEntityAnnotator
import pl.edu.pjatk.s10888.inl.pos.components.pipeline.POSAnnotator
import pl.edu.pjatk.s10888.inl.pos.components.verification.AnnotationVerificator
//import pl.edu.pjatk.s10888.inl.pos.components.verification.AnnotationVerificator
import pl.edu.pjatk.s10888.inl.pos.model.VerificationOutput
import java.util.concurrent.TimeUnit


fun main(args: Array<String>) {
    val trainer = TrainerMain()
    trainer.run()
}

class TrainerMain {

    private val corpusPath = File(this.javaClass.getResource("/nkjp_tab_l2.txt").toURI())
    private fun createCorpusReader() = UriCollectionReader.getCollectionReaderFromFiles(listOf(corpusPath))!!

    private val modelOutput = "./output/training"
    private val classificationOutput = "./output/classification"
    private val classificationOutputFile = "classification_output.txt"

    private val gson = GsonBuilder().setPrettyPrinting().create()

    fun run(): String {
        val selectedModel = listOf("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L").map { featureSet ->
            val stopwatch = Stopwatch.createStarted()
            train(featureSet, 0.0, 80.0)
            val validationOutput = validate(featureSet, 80.0, 90.0)
            stopwatch.stop()
            val duration = DurationFormatUtils.formatDurationHMS(stopwatch.elapsed(TimeUnit.MILLISECONDS))
            val validationFile = File("./output/classification/$featureSet/classification_validation.txt")
            printSummaryToFile(validationFile, featureSet, duration, validationOutput)
            validationOutput
        }.maxBy { it.second.f1 }

        println("Selected model: ${selectedModel!!.first}")
        println("Testing selected model...")
        val stopwatch = Stopwatch.createStarted()
        val result = validate(selectedModel.first, 90.0, 100.0)
        stopwatch.stop()
        val duration = DurationFormatUtils.formatDurationHMS(stopwatch.elapsed(TimeUnit.MILLISECONDS))
        val testReportFile = File("./output/classification/${selectedModel.first}/classification_test.txt")
        printSummaryToFile(testReportFile, selectedModel.first, duration, result)
        println("Result of the best model: ${result.second.f1}")
        return selectedModel.first
    }

    private fun train(featureSet: String, percentageStart: Double, percentageEnd: Double) {
        val builder = AggregateBuilder()
        val corpusReader = createCorpusReader()
        corpusReader.uimaContext.logger.setLevel(Level.OFF)
        UIMAFramework.getLogger(CrfSuiteWrapper::class.java).setLevel(Level.OFF)

        println("Preparing model $featureSet")
        builder.add(AnalysisEngineFactory.createEngineDescription(
                ViewCreatorAnnotator::class.java,
                ViewCreatorAnnotator.PARAM_VIEW_NAME,
                "default"))
        builder.add(AnalysisEngineFactory.createEngineDescription(
                CorpusReader::class.java,
                CorpusReader.PARAM_PERCENTAGE_START, percentageStart.toFloat(),
                CorpusReader.PARAM_PERCENTAGE_END, percentageEnd.toFloat()
        ))
        builder.add(NamedEntityAnnotator.getWriterDescription(File(modelOutput, featureSet).absolutePath, featureSet))
        SimplePipeline.runPipeline(corpusReader, builder.createAggregateDescription())

        println("Training model $featureSet")
        Train.main(File(modelOutput, featureSet).absolutePath)
    }

    private fun validate(featureSet: String, percentageStart: Double, percentageEnd: Double): Pair<String, VerificationOutput> {
        val builder = AggregateBuilder()
        val corpusReader = createCorpusReader()
        corpusReader.uimaContext.logger.setLevel(Level.OFF)

        builder.add(AnalysisEngineFactory.createEngineDescription(
                ViewCreatorAnnotator::class.java,
                ViewCreatorAnnotator.PARAM_VIEW_NAME,
                "default"))

        builder.add(AnalysisEngineFactory.createEngineDescription(
                CorpusReader::class.java,
                CorpusReader.PARAM_PERCENTAGE_START, percentageStart.toFloat(),
                CorpusReader.PARAM_PERCENTAGE_END, percentageEnd.toFloat()
        ))

        builder.add(NamedEntityAnnotator
                .getClassifierDescription(JarClassifierBuilder.getModelJarFile(
                        File(modelOutput, featureSet).absolutePath).path, featureSet))

        builder.add(AnalysisEngineFactory.createEngineDescription(
                AnnotatedOutputWriter::class.java,
                AnnotatedOutputWriter.PARAM_OUTPUT_DIRECTORY_NAME, File(classificationOutput, featureSet),
                AnnotatedOutputWriter.PARAM_OUTPUT_FILE_NAME, File(classificationOutputFile)
        ))

        println("Testing model $featureSet")
        SimplePipeline.runPipeline(corpusReader, builder.createAggregateDescription())

        return verify(featureSet, percentageStart, percentageEnd)
    }

    private fun verify(featureSet: String, percentageStart: Double, percentageEnd: Double): Pair<String, VerificationOutput> {
        val verificator = AnnotationVerificator()
        val verificationResult = verificator.verify(corpusPath,
                percentageStart,
                percentageEnd,
                File(File(classificationOutput, featureSet), classificationOutputFile))
        println("Model $featureSet, F1: ${verificationResult.f1}")
        return Pair(featureSet, verificationResult)
    }

    private fun printSummaryToFile(testReportFile: File, selectedModel: String, duration: String?, result: Pair<String, VerificationOutput>) {
        if (testReportFile.exists()) testReportFile.delete()
        testReportFile.printWriter()
                .use { w ->
                    w.println("Model $selectedModel")
                    w.println("Elapsed time: $duration")
                    w.println()
                    w.println(gson.toJson(result.second))
                }
    }
}