package pl.edu.pjatk.s10888.inl.pos

import org.apache.uima.fit.factory.AggregateBuilder
import org.apache.uima.fit.factory.AnalysisEngineFactory
import org.apache.uima.fit.pipeline.SimplePipeline
import org.apache.uima.util.Level
import org.cleartk.ml.jar.JarClassifierBuilder
import org.cleartk.opennlp.tools.SentenceAnnotator
import org.cleartk.token.tokenizer.TokenAnnotator
import org.cleartk.util.ae.UriToDocumentTextAnnotator
import org.cleartk.util.cr.UriCollectionReader
import pl.edu.pjatk.s10888.inl.pos.components.pipeline.AnnotatedOutputWriter
import pl.edu.pjatk.s10888.inl.pos.components.pipeline.NamedEntityAnnotator
import pl.edu.pjatk.s10888.inl.pos.components.pipeline.POSAnnotator
import java.io.File

fun main(args: Array<String>) {
    val tagger = TaggerMain()
    tagger.run("J")
}

@Suppress("UnstableApiUsage")
class TaggerMain {

    private val modelOutput = "./output/training"
    private val taggingOutput = "./output/tagging"
    private val taggingOutputFile = "tagging.txt"

    fun run(featureSet: String) {
        val builder = AggregateBuilder()
        val textPath = File(this.javaClass.getResource("/test_sentence.txt").toURI())
        val textReader = UriCollectionReader.getCollectionReaderFromFiles(listOf(textPath))!!
        textReader.uimaContext.logger.setLevel(Level.OFF)

        builder.add(UriToDocumentTextAnnotator.getDescription())
        builder.add(SentenceAnnotator.getDescription())
        builder.add(TokenAnnotator.getDescription())

        builder.add(POSAnnotator.getClassifierDescription(
                JarClassifierBuilder.getModelJarFile(File("./pos_model").absolutePath).path))

        builder.add(NamedEntityAnnotator
                .getClassifierDescription(JarClassifierBuilder.getModelJarFile(
                        File(modelOutput, featureSet).absolutePath).path, featureSet))

        builder.add(AnalysisEngineFactory.createEngineDescription(
                AnnotatedOutputWriter::class.java,
                AnnotatedOutputWriter.PARAM_OUTPUT_DIRECTORY_NAME, File(taggingOutput),
                AnnotatedOutputWriter.PARAM_OUTPUT_FILE_NAME, File(taggingOutputFile)
        ))

        println("Running model $featureSet")
        SimplePipeline.runPipeline(textReader, builder.createAggregateDescription())
    }
}