package pl.edu.pjatk.s10888.inl.pos.nerf

import com.google.gson.GsonBuilder
import pl.edu.pjatk.s10888.inl.pos.components.readers.AnnotatedFileParser
import pl.edu.pjatk.s10888.inl.pos.components.readers.AnnotatedFileReader
import pl.edu.pjatk.s10888.inl.pos.components.readers.AnnotatedFileSelector
import pl.edu.pjatk.s10888.inl.pos.components.verification.AnnotationVerificator
import pl.edu.pjatk.s10888.inl.pos.model.AnnotatedCorpusEntry
import java.io.File

fun main(args: Array<String>) {
    val validator = NerfValidator(true)
    validator.validateNerfTagging()
}


class NerfValidator(private val adjustData: Boolean) {

    private val corpusPath = File(this.javaClass.getResource("/nkjp_tab_l2.txt").toURI())
    private val outputFile = File("./nerf/test_data_nerf_annotated.txt")

    private val gson = GsonBuilder().setPrettyPrinting().create()

    fun adjustDataToMatchTokens() {
        val testData = readTestData(corpusPath, 90.0, 100.0)
        val outputData = readOutputData(outputFile).iterator()

        val adjustedOutputData = mutableListOf<AnnotatedCorpusEntry>()
        testData.forEach { testEntry ->
            val outputDataEntry = outputData.next()
            if (testEntry.word != outputDataEntry.word) {
                val nerTag = outputDataEntry.namedEntityTag
                var word = outputDataEntry.word
                var iterations = 0
                while (word != testEntry.word) {
                    val nextWord = outputData.next().word
                    val candidateWord1 = word + nextWord
                    val candidateWord2= "$word $nextWord"
                    word = when {
                        testEntry.word.startsWith(candidateWord1) -> candidateWord1
                        testEntry.word.startsWith(candidateWord2) -> candidateWord2
                        else -> throw RuntimeException("Can't adjust - no matches for prefix word. Expected ${testEntry.word}, got candidates: $candidateWord1, $candidateWord2")
                    }
                    iterations += 1
                    if (iterations > 10) {
                        throw RuntimeException("Can't adjust, too many iterations. Expected ${testEntry.word}, got so far: $word")
                    }
                }
                val newEntry = AnnotatedCorpusEntry(word = word, posTag = null, namedEntityTag = nerTag)
                adjustedOutputData.add(newEntry)
            } else {
                adjustedOutputData.add(outputDataEntry)
            }
        }

        outputFile.delete()
        outputFile.printWriter().use { w ->
            adjustedOutputData.forEach { entry -> w.println("${entry.word}\t${entry.namedEntityTag}")}
            w.println("&\t&")
        }
    }

    fun validateNerfTagging() {
        if (adjustData) adjustDataToMatchTokens()

        val verificator = AnnotationVerificator()
        val verificationResult = verificator.verify(corpusPath,
                90.0,
                100.0,
                outputFile)
        println("NERF, F1: ${verificationResult.f1}")

        val testReportFile = File("./nerf/classification_validation.txt")
        if (testReportFile.exists()) testReportFile.delete()
        testReportFile.printWriter()
                .use { w ->
                    w.println("Model NERF")
                    w.println()
                    w.println(gson.toJson(verificationResult))
                }
    }

    private fun readTestData(inputFile: File, percentageStart: Double, percentageEnd: Double): List<AnnotatedCorpusEntry> {
        val testDataReader = AnnotatedFileReader(inputFile)
        val parser = AnnotatedFileParser(testDataReader.readLinesFromCorpus())
        val selector = AnnotatedFileSelector(parser.recognizeSentences())
        val selectedSentences = selector.selectPercentage(percentageStart, percentageEnd)
        return selectedSentences.fold(mutableListOf()) { acc, annotatedSentence -> acc.addAll(annotatedSentence.annotatedWords); acc }
    }

    private fun readOutputData(classificationOutput: File): List<AnnotatedCorpusEntry> {
        val outputReader = AnnotatedFileReader(classificationOutput)
        val parser = AnnotatedFileParser(outputReader.readLinesFromTaggingOutput())
        return parser.recognizeSentences().flatMap { it.annotatedWords }
    }
}