package pl.edu.pjatk.s10888.inl.pos.components.transformers

import org.dom4j.Document
import java.io.File
import org.dom4j.io.SAXReader
import org.dom4j.tree.DefaultElement
import pl.edu.pjatk.s10888.inl.pos.components.transformers.CorpusTransformer.Companion.nonNamedEntityTag


class CorpusTransformer(private val inputPath: File, private val outputFile: File) {

    companion object {
        val nonNamedEntityTag = "non_name"
    }

    private val morfFile = "ann_morphosyntax.xml"
    private val namedFile = "ann_named.xml"

    private val reader = SAXReader()

    fun transform() {
        val subDirs = inputPath.listFiles { current, _ ->  current.isDirectory}
        val subdirsCount = subDirs.size

        val rawCorpus = subDirs.withIndex().flatMap {
            val subdir = it.value
            val morfFile = subdir.resolve(morfFile)
            val namedFile = subdir.resolve(namedFile)

            if (morfFile.exists()) {
                val morphSentences = readMorphSentences(reader.read(morfFile))
                val namedWords = if (namedFile.exists()) readNamedWords(reader.read(namedFile)) else emptyList()
                val corpus = mergeMorphsWithNamedWords(morphSentences, namedWords)

                print("\rReading file: ${it.index+1}/$subdirsCount")

                corpus.map { sentence -> sentence.map {
                    "${it.morphWord.baseForm}\t${it.morphWord.getPOS()}\t${it.namedWord?.getNamedEntityTag() ?: nonNamedEntityTag}"
                }}
            } else emptyList()
        }
        println()
        if (outputFile.exists()) outputFile.delete()
        outputFile.printWriter().use { writer ->
            val sentenceCount = rawCorpus.size
            rawCorpus.forEachIndexed { index, sentence ->
                print("\rWriting sentence ${index+1}/$sentenceCount")
                sentence.forEach { writer.println(it)}
                writer.println("&\t&\t&")
            }
        }
    }

    private fun mergeMorphsWithNamedWords(morphSentences: List<MorphSentence>, namedWords: List<NamedWord>): List<List<CorpusWord>> {
        return morphSentences.map {
            it.words.map { word ->
                CorpusWord(word, namedWords.find { it.target == word.id })
            }
        }
    }

    private fun readNamedWords(namedDocument: Document): List<NamedWord> {
        val entries = namedDocument.selectNodes("" +
                "//teiCorpus" +
                "/*[local-name() = 'TEI']" +
                "/*[local-name() = 'text']" +
                "/*[local-name() = 'body']" +
                "/*[local-name() = 'p']" +
                "/*[local-name() = 's']" +
                "/*[local-name() = 'seg']")

        return entries.filter { (it as DefaultElement).elements()
                .find {it.name == "ptr" && it.attribute("target").stringValue.contains("ann_morphosyntax.xml") }!= null}
                .map { entry ->
                    val forms = (entry as DefaultElement).element("fs").elements()

                    val type = forms.find { it.name == "f" && it.attribute("name").stringValue == "type" }!!
                            .element("symbol")
                            .attribute("value").stringValue

                    val subtype = forms.find { it.name == "f" && it.attribute("name").stringValue == "subtype" }
                            ?.element("symbol")
                            ?.attribute("value")?.stringValue

                    val target = (entry.element("ptr") as DefaultElement)
                            .attribute("target").stringValue.split("#").last()
                    NamedWord(target, type, subtype)
                }
    }

    private fun readMorphSentences(morfDocument: Document): List<MorphSentence> {
        val sentences = morfDocument.selectNodes("" +
                "//teiCorpus" +
                "/*[local-name() = 'TEI']" +
                "/*[local-name() = 'text']" +
                "/*[local-name() = 'body']" +
                "/*[local-name() = 'p']" +
                "/*[local-name() = 's']")

        return sentences.map { sentence ->
            val words = (sentence as DefaultElement).elements().filter { it.name == "seg" }.map { it as DefaultElement }
            MorphSentence(words.map { word ->
                val id = word.attribute("id").value
                val forms = word.elements().first().elements()

                val baseForm = forms.find { it.name == "f" && it.attribute("name").stringValue == "orth" }!!
                        .element("string").stringValue

                val disamb = forms.find { it.name == "f" && it.attribute("name").stringValue == "disamb" }!!
                        .element("fs")
                        .elements()
                        .find { it.name == "f" && it.attribute("name").stringValue == "interpretation" }!!
                        .element("string").stringValue

                MorphWord(id, baseForm, disamb)
            })
        }
    }
}

data class MorphWord(val id: String, val baseForm: String, val disamb: String) {
    fun getPOS(): String {
        return disamb.split(":")[1]
    }
}
data class MorphSentence(val words: List<MorphWord>)
data class NamedWord(val target: String, val type: String, val subtype: String?) {
    fun getNamedEntityTag(): String {
        if (type in listOf("date", "time")) return nonNamedEntityTag
        return type + (subtype?.prependIndent("_") ?: "")
    }
}
data class CorpusWord(val morphWord: MorphWord, val namedWord: NamedWord?)

fun main(args: Array<String>) {
    val transformer = CorpusTransformer(File("./NKJP-PodkorpusMilionowy-1.2"), File("./data/nkjp_tab_l1.txt"))
    transformer.transform()
}