package pl.edu.pjatk.s10888.inl.pos.nerf

import pl.edu.pjatk.s10888.inl.pos.components.readers.AnnotatedFileParser
import pl.edu.pjatk.s10888.inl.pos.components.readers.AnnotatedFileReader
import pl.edu.pjatk.s10888.inl.pos.components.readers.AnnotatedFileSelector
import java.io.File

fun main(args: Array<String>) {
    val extractor = NerfTestDataExtractor()
    extractor.extractDataForNerf(90.0, 100.0)
}

class NerfTestDataExtractor {
    private val file = File(this.javaClass.getResource("/nkjp_tab_l2.txt").toURI())
    private val outputFile = File("./nerf/test_data_for_nerf.txt")

    fun extractDataForNerf(percentageStart: Double, percentageEnd: Double) {
        val reader = AnnotatedFileReader(file)
        val lines = reader.readLinesFromCorpus()

        val parser = AnnotatedFileParser(lines)
        val sentences = parser.recognizeSentences()

        val selector = AnnotatedFileSelector(sentences)
        val selectedSentences = selector.selectPercentage(percentageStart, percentageEnd)

        if (outputFile.exists()) outputFile.delete() else outputFile.createNewFile()
        outputFile.printWriter().use { w -> selectedSentences.forEach { w.println(it.makeSentence()) } }
    }
}