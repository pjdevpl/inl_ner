package pl.edu.pjatk.s10888.inl.pos.components.pipeline

import org.apache.uima.UimaContext
import org.apache.uima.analysis_engine.AnalysisEngineDescription
import org.apache.uima.fit.factory.AnalysisEngineFactory
import org.apache.uima.fit.util.JCasUtil
import org.apache.uima.jcas.JCas
import org.cleartk.ml.CleartkSequenceAnnotator
import org.cleartk.ml.Instance
import org.cleartk.ml.crfsuite.CrfSuiteStringOutcomeDataWriter
import org.cleartk.ml.feature.extractor.CleartkExtractor
import org.cleartk.ml.feature.extractor.CoveredTextExtractor
import org.cleartk.ml.feature.function.*
import org.cleartk.ml.jar.DefaultDataWriterFactory
import org.cleartk.ml.jar.DirectoryDataWriterFactory
import org.cleartk.ml.jar.GenericJarClassifierFactory
import org.cleartk.token.type.Sentence
import org.cleartk.token.type.Token
import pl.edu.pjatk.s10888.inl.pos.utils.ParamsInitializer


class POSAnnotator : CleartkSequenceAnnotator<String>() {

    companion object {
        fun getWriterDescription(outputDirectory: String): AnalysisEngineDescription {
            return AnalysisEngineFactory.createEngineDescription(
                    POSAnnotator::class.java,
                    DirectoryDataWriterFactory.PARAM_OUTPUT_DIRECTORY,
                    outputDirectory,
                    DefaultDataWriterFactory.PARAM_DATA_WRITER_CLASS_NAME,
                    CrfSuiteStringOutcomeDataWriter::class.java.name)
        }

        fun getClassifierDescription(modelFileName: String): AnalysisEngineDescription {
            return AnalysisEngineFactory.createEngineDescription(
                    POSAnnotator::class.java,
                    GenericJarClassifierFactory.PARAM_CLASSIFIER_JAR_PATH,
                    modelFileName)
        }
    }

    private val tokenFeatureExtractor = FeatureFunctionExtractor(
            CoveredTextExtractor<Token>(),
            LowerCaseFeatureFunction(),
            CapitalTypeFeatureFunction(),
            NumericTypeFeatureFunction(),
            CharacterNgramFeatureFunction(CharacterNgramFeatureFunction.Orientation.RIGHT_TO_LEFT, 0, 2),
            CharacterNgramFeatureFunction(CharacterNgramFeatureFunction.Orientation.RIGHT_TO_LEFT, 0, 3))

    private val contextFeatureExtractor = CleartkExtractor<Token, Token>(
            Token::class.java,
            CoveredTextExtractor(),
            CleartkExtractor.Preceding(2),
            CleartkExtractor.Following(2))

    override fun initialize(context: UimaContext) {
        super.initialize(context)
        ParamsInitializer.initializeParameters(this, context)
    }

    override fun process(aJCas: JCas?) {
        val sentences = JCasUtil.select(aJCas, Sentence::class.java)
        val sentencesCount = sentences.size
        sentences.forEachIndexed { index, sentence ->
            val tokens = JCasUtil.selectCovered(aJCas, Token::class.java, sentence)

            val tokenInstances = tokens.map { token ->
                val tokenFeatures = this.tokenFeatureExtractor.extract(aJCas, token)
                val tokenContextFeatures = this.contextFeatureExtractor.extractWithin(aJCas, token, sentence)
                Instance(token.pos, tokenFeatures + tokenContextFeatures)
            }

            print("\rPOS, classifying: ${index+1}/$sentencesCount")

            val predictedOutcomes = this.classifier.classify(tokenInstances.map { it.features })
            val tokensIter = tokens.iterator()
            for (outcome in predictedOutcomes) {
                tokensIter.next().pos = outcome
            }
        }
        println()
    }
}