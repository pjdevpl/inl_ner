package pl.edu.pjatk.s10888.inl.pos.model

data class DetailedMetrics(val tp: Int, val fp: Int, val tn: Int, val fn: Int)
data class Metrics(val precision: Double, val recall: Double)
data class VerificationOutput(val labelDetailedMetrics: Map<String, DetailedMetrics>,
                              val labelMetrics: Map<String, Metrics>,
                              val precision: Double,
                              val recall: Double,
                              val f1: Double)